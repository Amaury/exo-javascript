describe('CompterMots', function () {
    it('Chaine vide', function () {
        expect(CompterMots("")).toEqual(0);
    });
    it('Chaine de un mot avec un blanc avant', function () {
        expect(CompterMots("    test")).toEqual(1);
    });
    it('Chaine de un mot avec un blanc après', function () {
        expect(CompterMots("test   ")).toEqual(1);
    });
    it('Chaine de un mot avec un blanc avant et après', function () {
        expect(CompterMots("    test   ")).toEqual(1);
    });
    it('Chaine de 4 mots avec blancs et tabulation', function () {
        expect(CompterMots("   ceci est	 un test   ")).toEqual(4);
    });
    it('Chaine de 4 mots avec blanc et un retour à la ligne', function () {
        expect(CompterMots("   ceci est\nun test   ")).toEqual(4);
    });
    it('Chaine de 4 mots normal', function () {
        expect(CompterMots("ceci est un test.")).toEqual(4);
    });
});
